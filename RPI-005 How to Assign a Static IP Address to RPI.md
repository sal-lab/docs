**How to Assign a Static IP Address to RPI**

Static IP assignment for RPI helps the device to be remotely accessed easily in a local network. The following steps should be followed:

* `sudo nano /etc/dhcpcd.conf` (edit the dhcp config file)

At the end of the file, write the following lines of code:

`interface wlan0`
`static ip_address=192.168.8.143/24` (sample IP Address)  
`static router=192.168.8.1` (sample gateway address/router address)  
`static domain_name_server=192.168.8.1`  

Press `ctrl+x` and save changes without renaming the file.  
Reboot RPI to apply changes

* `sudo reboot -h now`

You're done!

**For Multiple Networks - Host Specific Configuration**

Most of the time we rely on `dhcpcd` for RPi to get assigned an IP address. 
Sometimes your static IP may not be appropriate to the network's subnet mask. 
We can use ARP to probe hosts based on MAC or IP address before DHCP resolution. 
Open `/etc/dhcpcd.conf` and type the following:  

`interface wlan0`  
`arping 192.168.1.1`  
`arping 10.1.31.1`  
`arping 192.168.8.1`  
  
`profile 192.168.1.1`  
`static ip_address=192.168.1.101/24`  
`static routers=192.168.1.1`  
`static domain_name_server=192.168.1.1`  
  
`profile 10.1.31.1`  
`static ip_address=10.1.31.89/24`  
`static routers=10.1.31.1`  
`static domain_name_server=10.1.7.3`  
  
`profile 192.168.8.1`  
`static ip_address=192.168.8.31/24`  
`static routers=192.168.8.1`  
`static domain_name_server=192.168.8.1`   

Close the file and reboot.  



