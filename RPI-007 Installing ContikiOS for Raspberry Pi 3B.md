**Installing ContikiOS in Raspberry Pi 3B**

The following instructions will help you setup Contiki-OS in Raspberry Pi. This document was last update on May 2018. Note that this installation was made for developing programs for Firefly Wireless Sensors

If you have installed the tools mentioned in T1-001, download the following toolchains

* `sudo apt-get install gcc-arm-none-eabi gdb-arm-none-eabi`

Clone the contiki folder to the default folder.

* `cd`
* `git clone https://github.com/contiki-os/contiki.git`

Create a contiki environment file:

`export CONTIKI="/home/pi/contiki`  
`export TARGET=zoul`  
`export BOARD=firefly`  

`echo "Setting up Contiki OS on source path $CONTIKI..."`  

Save the environment file as `contiki.env` and open the bashrc file.

* `sudo nano /home/pi/.bashrc`

At the end of the file, input the following line: `source /home/pi/contiki/contiki.env`
Reboot Raspberry Pi.

The default *serialdump-linux* file may present problems when uploading codes into the Firefly motes, so do the following remedy:

* `cd /home/pi/contiki/tools/sky`  
* `rm serialdump-linux`
* `make serialdump`
* `mv serialdump serialdump-linux`

At this point, you may now begin develop Firefly wireless sensors using ContikiOS.

*Special thanks to Engr. Patrick Jason Piera for his contributions in installing ContikiOS in Raspberry Pi*