**How to Change Screen Resolution in RPI**

To change the screen resolution of the RPI display to match the monitor attached to the device through the HDMI (or when device was opened through VNC), the following process must be followed:

* `sudo nano /boot/config.txt`

Locate the HDMI settings in the code then add the following lines:

* `hdmi_ignore_edid=0xa5000080`
* `hdmi_group=2`
* `hdmi_mode=39` (1366x768 resolution)

Press `ctrl+x` and save changes without renaming the file.

Restart the device to apply changes.

* `sudo reboot -h now`

You're done!

*p.s. For list of HDMI modes, see attached pdf in this repository*