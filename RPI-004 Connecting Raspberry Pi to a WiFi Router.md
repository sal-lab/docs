**Connecting Raspberry Pi to a WiFi Router**

Follow the following steps in order to connect your RPI wirelessly to a WiFi router.

First, we need to open the following configuration file. It lists all access points that the Raspberry Pi has connected in its history.

*`sudo /etc/wpa_supplicant/wpa_supplicant.conf`

At the end of the file, add an access point with the following format:

`network={`  
`ssid="Enter-your-ssid-here"`  
`psk="Enter-password-here"`  
`}`  

Save the file (`ctrl+x`)and reboot the Raspberry Pi.

* `sudo reboot`

Once rebooted, you should be able to connect to the specified WiFi access point.