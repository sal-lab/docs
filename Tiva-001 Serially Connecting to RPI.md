**Serially Connecting to RPI**

1. Install python3-pip: `sudo apt-get install python3-pip`
2. Enable RPI's UART by going to the configuration page (`sudo raspi-config`), select
*Interfacing Options* and select *Serial*.
3. Write a python code that can communicate with RPI terminal/serial